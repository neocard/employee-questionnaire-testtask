# o/ devolon - Test task - Employee questionnaire:

We need a questionnaire to ask our employees to rate the following 5 different options:

1. Work-life balance
2. Salary
3. Senior management
4. Self-development opportunities
5. Organization-development opportunities.

### The goal of the project:

in this sense, we need to have the following functionalities:

1. Employees should fill the questionnaire
2. Employer should be able to see the questionnaire for each employee
3. Employer needs to have some statistics
4. Employee should be able to edit his/her answers within one week after the first submission (with a unique link).

### Structure (suggestion):

#### The database you should have one table/collection (questionnaire) with the following fields:
field-name | type | comments 
--- | --- | ---
id | unsigned integer/string | unique for each reccord
created_at | date-time string | 
updated_at | date-time string |
name | string | should be able to carry at most 255 characters
work_life_balance_rate | unsigned tiny integer | should be between 1 to 5
salary_rate | unsigned tiny integer | should be between 1 to 5
senior_management_rate | unsigned tiny integer | should be between 1 to 5
self_development_rate | unsigned tiny integer | should be between 1 to 5
organization_development_rate | unsigned tiny integer | should be between 1 to 5
summary | text | should be able to carry at most 65535 characters

####  APIs:

1. To fill in questionnaire you need to provide an API which will receive data within a `POST` request body and will generate a unique link to edit the questionnaire (the link should be valid for only one week)
2. Create two endpoints for admin, a `GET` API to retrieve all questionnaires and one other `GET` endpoint to receive the average rate for each option in our questionnaire.
3. by calling the generated URL (from the first step) with `PUT` method, the user should be able to edit only rates and summaries.

#### Exceptions and validations:

If an end-user (front-end) tries to send invalid data, you should send a response with a status code of `422` and a proper message.
If a user tries to edit a not found questionnaire you should return `404` with a proper message
In the case that a user tries to call edit link after one week, the result should be in `403` status code and of course with a proper message.

At the end of the day, please provide proper documentation (postman or swagger) for your APIs

### Bouns points:

1. Write your codes in typescript.
2. Dockerize your application (orchestrate your application with docker-compose).
3. Implement a simple front-end (using react) for your application.
4. Implement tests for your APIs and codes in the backend side.

To deliver your task please create a git repository (GitHub/GitLab/bitbucket) and share it with us.

